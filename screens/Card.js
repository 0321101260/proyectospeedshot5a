import React from "react";
import styled from "styled-components";

const CardContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 300px;
    padding: 20px;
    border: 1px solid #ccc;
    border-radius: 15px;
    margin: 20px;
    box-shadow: 0 2px 8px rgba(0, 0, 0, 0.2);
`;

const CardImage = styled.img`
    width: 100%;
    height: auto;
    object-fit: cover;
    border-radius: 10px;
`;

const CardTitle = styled.h2`
    margin: 15px 0;
    font-size: 24px;
    color: #333;
`;

const CardDescription = styled.p`
    font-size: 16px;
    color: #666;
`;

function Card({ imageURL = "https://i.ibb.co/R0Y8T8r/nike19.png", title = "nike", description = "Default Description" }) {
    return (
        <CardContainer>
            <CardImage src={imageURL} alt={title} />
            <CardTitle>{title}</CardTitle>
            <CardDescription>{description}</CardDescription>
        </CardContainer>
    );
}

export default Card;
