import React, { useState, useContext, useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { EmailContext } from '../contexts/EmailContext';

const PitcherScreen = () => {
  const [speeds, setSpeeds] = useState([]);
  const [userNumber, setUserNumber] = useState(null);
  const { email } = useContext(EmailContext);
  const fetchSpeed = async () => {
    try {
      const response = await fetch(`http://192.168.43.131:3000/api/mediciones?correo=${email}`);
      if (!response.ok) {
        throw new Error('Error al obtener los datos');
      }
      const data = await response.json();
      const newSpeeds = data.map(item => ({
        time: new Date(item.fecha_hora).toISOString().slice(0, 19).replace('T', ' '),
        speed: item.velocidadmedida,
      }));
      setSpeeds(newSpeeds.slice(-15).reverse()); // Tomar las últimas 10 velocidades y luego invertir el orden
    } catch (error) {
      console.error('Error en la solicitud de la API: ', error);
    }
  };

  const fetchUserNumber = async () => {
    try {
      const response = await fetch(`http://192.168.43.131:3000/api/numero-usuario?correo=${email}`);
      if (!response.ok) {
        throw new Error('Error al obtener el número de usuario');
      }
      const data = await response.json();
      setUserNumber(data.numero);
    } catch (error) {
      console.error('Error en la solicitud de la API de número de usuario: ', error);
    }
  };

  useEffect(() => {
    const fetchInterval = setInterval(() => {
      fetchSpeed();
      fetchUserNumber();
    }, 5000);

    return () => clearInterval(fetchInterval);
  }, [email]);

  return (
    <View style={styles.container}>
      <Text style={styles.userNumber}>Número de Usuario: {userNumber}</Text>
      {speeds.length > 0 && (
        <>
          <Text style={styles.lastSpeedTitle}>Última velocidad registrada</Text>
          <View style={styles.speedContainer}>
            <Text style={styles.speedValue}>{Math.round(speeds[0].speed)}</Text>
            <Text style={styles.speedUnit}>MPH</Text>
          </View>
        </>
      )}
      {speeds.length > 0 && (
        <ScrollView horizontal contentContainerStyle={styles.tableContainer}>
          <View style={styles.table}>
            {speeds.map((speedItem, index) => (
              <Text key={index} style={styles.tableText}>
                {Math.round(speedItem.speed)}
              </Text>
            ))}
          </View>
          <View style={styles.table}>
            {speeds.map((speedItem, index) => (
              <Text key={index} style={styles.tableText}>
                {speedItem.time}
              </Text>
            ))}
          </View>
        </ScrollView>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 40,
  },
  userNumber: {
    fontSize: 20,
    marginBottom: 20,
  },
  lastSpeedTitle: {
    fontWeight: 'bold',
    fontSize: 24,
    marginBottom: 20,
  },
  speedContainer: {
    width: 140,
    height: 140,
    padding: 10,
    borderWidth: 5,
    borderColor: 'red',
    borderRadius: 70,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
  },
  speedValue: {
    fontSize: 64,
  },
  speedUnit: {
    fontSize: 32,
  },
  tableContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: 20,
  },
  table: {
    borderWidth: 1,
    borderColor: 'black',
    textAlign: 'center',
    fontSize: 16,
    width: '48%',
  },
  tableText: {
    borderWidth: 1,
    borderColor: 'black',
    padding: 5,
  },
});

export default PitcherScreen;
