import React, { useState, useContext, useEffect } from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';
import { VictoryLine, VictoryScatter, VictoryChart, VictoryAxis, VictoryLabel } from 'victory-native';
import { EmailContext } from '../contexts/EmailContext';

const MonthlyView = () => {
  const [data, setData] = useState([]);
  const { email } = useContext(EmailContext);

  useEffect(() => {
    fetch(`http://192.168.43.131:3000/api/mensual?correo=${email}`)
      .then(response => response.json())
      .then(result => {
        if (Array.isArray(result)) {
          const limitedData = result.slice(-25); // Limitando a 25 registros
          setData(limitedData);
        }
      })
      .catch(error => {
        console.error('Error al obtener los datos:', error);
      });
  }, [email]);

  const speeds = data.map(item => item.velocidadmedida);

  const convertToPST = (utcDate) => {
    const date = new Date(utcDate);
    const offset = date.getTimezoneOffset() + 600;
    date.setMinutes(date.getMinutes() + offset - 1440);
    return date.toISOString().slice(0, 19).replace('T', ' ');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Gráfica de Velocidades Mensuales</Text>
      <VictoryChart>
        <VictoryLine
          data={speeds.map((speed, index) => ({ x: index + 1, y: speed }))}
          style={{
            data: { stroke: 'rgba(204, 0, 0, 1)' },
          }}
        />
        <VictoryScatter
          data={speeds.map((speed, index) => ({ x: index + 1, y: speed }))}
          size={5}
          style={{
            data: { fill: 'rgba(204, 0, 0, 1)' },
          }}
        />
        <VictoryAxis
          tickValues={Array.from({ length: speeds.length }, (_, i) => i + 1)}
          tickLabelComponent={<VictoryLabel angle={45} />} // Rotar etiquetas para evitar superposición
          tickCount={10} // Ajustar el número de etiquetas en el eje X
        />
      </VictoryChart>
      <ScrollView style={styles.tableContainer}>
        <View style={styles.tableHeader}>
          <Text style={styles.headerText}>#</Text>
          <Text style={styles.headerText}>Fecha y Hora</Text>
          <Text style={styles.headerText}>Velocidad</Text>
        </View>
        {data.map((item, index) => (
          <View key={index} style={styles.tableRow}>
            <Text style={styles.rowText}>{index + 1}</Text>
            <Text style={styles.rowText}>{convertToPST(item.fecha_hora)}</Text>
            <Text style={styles.rowText}>{item.velocidadmedida}</Text>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  tableContainer: {
    flex: 1,
    width: '100%',
    marginBottom: 10,
  },
  tableHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    paddingVertical: 5,
    backgroundColor: '#f2f2f2',
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  tableRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    paddingVertical: 5,
  },
  rowText: {
    fontSize: 14,
  },
});

export default MonthlyView;
