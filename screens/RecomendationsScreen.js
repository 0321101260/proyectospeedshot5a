import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from 'react-native';

const RecomendationsScreen = () => {
  const [recomendations, setRecomendations] = useState([]);

  useEffect(() => {
    // Función para cargar las recomendaciones desde la API
    const fetchRecomendations = async () => {
      try {
        const response = await fetch('http://192.168.43.131:3000/api/recomendaciones');
        const data = await response.json();
        setRecomendations(data);
      } catch (error) {
        console.error('Error al obtener las recomendaciones:', error);
      }
    };

    // Llamando a la función al montar el componente
    fetchRecomendations();
  }, []);

  return (
    <ScrollView style={styles.container}>
      <Image source={require('../img/speedshot.png')} style={styles.image} />
      {recomendations.map((item, index) => (
        <View key={index} style={styles.recomendation}>
          <Text style={styles.text}>{item.contenido}</Text>
        </View>
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: 'white', // Fondo en blanco
  },
  recomendation: {
    marginVertical: 10,
    padding: 15,
    borderRadius: 5,
    backgroundColor: '#f0f0f0',
    alignItems: 'center', // Centrar contenido horizontalmente
  },
  text: {
    fontSize: 18,
    textAlign: 'justify', // Justificar el texto
  },
  image: {
    width: 200,
    height: 150,
    alignSelf: 'center', // Centrar la imagen horizontalmente
    marginTop: 10,
  },
});

export default RecomendationsScreen;
