import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

const TrainingScreen = ({ navigation }) => {
  const handleSportSelection = (sport) => {
    console.log('Deporte seleccionado:', sport);

    // Navega a la pantalla "Pitcher" cuando se selecciona "Pitcher"
    if (sport === 'Pitcher') {
      navigation.navigate('Pitcher');
    }
  };

  return (
    <View style={[styles.container, { backgroundColor: 'white' }]}>
      <TouchableOpacity style={styles.button} onPress={() => handleSportSelection('Pitcher')}>
        <Text style={styles.buttonText}>Pitcher​</Text>
      </TouchableOpacity>
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: '#cc0000',
    padding: 10,
    marginVertical: 10,
    borderRadius: 5,
    width: 280,
    height: 150,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 25,
    fontWeight: 'bold',
  },
});

export default TrainingScreen;