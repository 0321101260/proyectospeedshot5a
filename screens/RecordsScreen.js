import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

const WeeklyMonthlyScreen = ({ navigation }) => {
  const handleSemanal = () => {
    // Navegar a la pantalla de la vista semanal
    navigation.navigate('Semanal');
  };
  
  const handleMensual = () => {
    // Navegar a la pantalla de la vista mensual
    navigation.navigate('Mensual');
  };

  return (
    <View style={[styles.container, { backgroundColor: 'white' }]}>
      <TouchableOpacity style={styles.button} onPress={handleSemanal}>
        <Text style={styles.buttonText}>Semanal</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={handleMensual}>
        <Text style={styles.buttonText}>Mensual</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  button: {
    padding: 10,
    marginVertical: 10,
    borderRadius: 5,
    width: 250,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#cc0000',
  },
  buttonText: {
    color: '#fff',
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default WeeklyMonthlyScreen;
