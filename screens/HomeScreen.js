import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet, ImageBackground } from 'react-native';

const HomeScreen = ({ navigation }) => {
  const handleTraining = () => {
    // Navegar a la pantalla de entrenamiento
    navigation.navigate('Training');
  };
  
  const handleRecords = () => {
    // Navegar a la pantalla de registros
    navigation.navigate('Records');
  };

  const handleRecom = () => {
    // Navegar a la pantalla de registros
    navigation.navigate('Recomendations');
  };

  const handleAccount = () => {
    // Navegar a la pantalla de cuenta
    navigation.navigate('Account');
  };

  return (
    <ImageBackground source={require('../img/estadiobeis.jpg')} style={styles.background}>
      <View style={styles.container}>
        <TouchableOpacity style={styles.button} onPress={handleTraining}>
          <Text style={styles.buttonText}>Entrenamiento</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={handleRecords}>
          <Text style={styles.buttonText}>Registros</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={handleRecom}>
          <Text style={styles.buttonText}>Recomendaciones</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={handleAccount}>
          <Text style={styles.buttonText}>Cuenta</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover', // Puedes ajustar esto según tus necesidades
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    padding: 10,
    marginVertical: 10,
    borderRadius: 5,
    width: 250,
    height: 100,
    alignItems: 'center', 
    justifyContent: 'center', 
    backgroundColor: '#cc0000',
  },
  buttonText: {
    color: '#fff',
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default HomeScreen;
