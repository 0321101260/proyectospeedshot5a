import React, { useEffect } from 'react';
import { View, Text, ActivityIndicator, Image, StyleSheet } from 'react-native';

const LoadingScreen = ({ navigation }) => {
  useEffect(() => {
    // Simula una espera de 3 segundos antes de navegar a la pantalla de inicio de sesión
    setTimeout(() => {
      navigation.replace('Login');
    }, 3000);
  }, []);

  return (
    <View style={styles.container}>
      <Image source={require('../img/speedshot.png')} style={styles.image} />
      <ActivityIndicator size="large" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 400,
    height: 400,
    marginBottom: 15,
  },
});

export default LoadingScreen;
