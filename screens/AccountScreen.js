import React, { useContext, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native'; 
import { EmailContext } from '../contexts/EmailContext';

const AccountScreen = ({ navigation }) => {
  const [user, setUser] = useState(null);
  const { email } = useContext(EmailContext);

  useEffect(() => {
    fetchUserDataFromAPI()
      .then(userData => setUser(userData))
      .catch(error => console.log(error));
  }, [email]);

  const fetchUserDataFromAPI = () => {
    return new Promise((resolve, reject) => {
      fetch(`http://192.168.43.131:3000/api/userinfo?correo=${email}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then(response => response.json())
        .then(data => {
          const { numero, nombre, apellido, nom_usuario, correo } = data;
          resolve({ numero, nombre, apellido, nom_usuario, correo });
        })
        .catch(error => {
          reject(error);
        });
    });
  };

  const handleLogout = () => {
    navigation.reset({
      index: 0,
      routes: [{ name: 'Login' }],
    });
  };

  if (!user) {
    return (
      <View style={styles.container}>
        <Text>Cargando...</Text>
      </View>
    );
  }

  return (
    <View style={[styles.container, { backgroundColor: 'white' }]}>
      {/* Agregamos la imagen */}
      <Image
        source={require('../img/pitcher2.jpeg')} 
        style={styles.userImage}
      />
      <View style={styles.userInfo}>
        
        <Text style={styles.label}>ID:</Text>
        <Text style={styles.data}>{user.numero}</Text>

        <Text style={styles.label}>Nombre:</Text>
        <Text style={styles.data}>{user.nombre}</Text>

        <Text style={styles.label}>Apellido:</Text>
        <Text style={styles.data}>{user.apellido}</Text>

        <Text style={styles.label}>Nombre de usuario:</Text>
        <Text style={styles.data}>{user.nom_usuario}</Text>

        <Text style={styles.label}>Correo:</Text>
        <Text style={styles.data}>{user.correo}</Text>
      </View>
      <TouchableOpacity style={styles.button} onPress={handleLogout}>
        <Text style={styles.buttonText}>Cerrar Sesión</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  userImage: {
    width: 150,   // Cambia el valor para hacer la imagen más ancha
    height: 150,  // Cambia el valor para hacer la imagen más alta
    borderRadius: Math.min(200, 200) / 2,  // Ajusta el valor del borderRadius si es necesario
    borderWidth: 2, // Ancho del borde rojo
    borderColor: 'red', // Color del borde rojo
    marginBottom: 20,
  },
  userInfo: {
    alignItems: 'center',
    marginBottom: 20,
  },
  label: {
    fontSize: 18,
    marginBottom: 5,
  },
  data: {
    fontSize: 22,
    marginBottom: 10,
  },
  button: {
    backgroundColor: '#2ecc71',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default AccountScreen;
