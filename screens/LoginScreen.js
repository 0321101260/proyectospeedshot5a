import React, { useState, useContext } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { EmailContext } from '../contexts/EmailContext';

const LoginScreen = ({ navigation }) => {
  const { email, setEmail } = useContext(EmailContext);
  const [password, setPassword] = useState('');
  const [showAlert, setShowAlert] = useState(false);

  const handleLogin = () => {
    if (email === '' || password === '') {
      setShowAlert(true);
    } else {
      fetch('http://192.168.43.131:3000/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          correo: email,
          password: password,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.message === 'Inicio de sesión exitoso') {
            setEmail(email);
            navigation.replace('Home');
          } else {
            console.log('Credenciales inválidas');
          }
        })
        .catch((error) => {
          console.error('Error:', error);
        });
    }
  };

  const handleRegister = () => {
    navigation.navigate('Register');
  };

  return (
    <View style={[styles.container, { backgroundColor: 'white' }]}>
      <Image source={require('../img/speedshot.png')} style={styles.image} />
      <TextInput
        style={styles.input}
        placeholder="Correo electrónico"
        value={email}
        onChangeText={setEmail}
      />
      <TextInput
        style={styles.input}
        placeholder="Contraseña"
        secureTextEntry
        value={password}
        onChangeText={setPassword}
      />
      {showAlert && (
        <Text style={styles.alertText}>Por favor, ingresa tu correo y contraseña</Text>
      )}
      <TouchableOpacity style={styles.button} onPress={handleLogin}>
        <Text style={styles.buttonText}>Iniciar sesión</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.registerLink} onPress={handleRegister}>
        <Text style={styles.registerLinkText}>No tienes cuenta? Regístrate</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    width: 400,
    height: 400,
    marginBottom: 10,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  input: {
    width: '30%',
    height: 35,
    borderColor: 'gray',
    borderWidth: 2,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  button: {
    backgroundColor: '#cc0000',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 19,
  },
  registerLink: {
    marginTop: 10,
  },
  registerLinkText: {
    color: 'blue',
    textDecorationLine: 'underline',
  },
  alertText: {
    color: 'red',
    marginBottom: 10,
  },
});

export default LoginScreen;
