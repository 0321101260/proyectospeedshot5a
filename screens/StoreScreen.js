import React from 'react';
import styled from 'styled-components';
import Card from './Card'; // Asegúrate de que la ruta de importación sea la correcta

const AppContainer = styled.div`
    display: flex;
    flex-direction: column; /* Cambio a columna para ajustar el texto principal arriba */
    justify-content: flex-start; /* Alinea el contenido arriba */
    align-items: center;
    height: 100vh;
    padding: 50px;
    box-sizing: border-box;
    background: white; /* Cambio del color de fondo a blanco */
`;

const MainText = styled.h1`
    text-align: center;
    margin-bottom: 20px;
`;

const Description = styled.p`
    text-align: justify; /* Agregado para justificar el texto */
`;

function App() {
    return (
        <AppContainer>
            <MainText>Productos de Béisbol</MainText>
            <Card imageURL="https://i.ibb.co/Rgx46B0/pelotasb.jpg" title="Rawlings cubeta de Bolas" description="Fácil de agarrar y lanzar gracias a las cubiertas de cuero sintético con centros de corcho sólido y goma. El resistente balde blanco incluye el logotipo de MLB y el parche rojo de Rawlings." />
            <Card imageURL="https://i.ibb.co/Wcm93cD/guante.jpg" title="San Diego HEART OF THE HIDE GLOVE" description="¡Si eres fanático de los Padres, debes echarle un vistazo a esto! El guante Heart of the Hide de los San Diego Padres para el año 2023 fue hecho especialmente para cualquier fanático de los Padres o coleccionista." />
            <Card imageURL="https://i.ibb.co/FY1ZFcG/bat.jpg" title="MLB PRIME SIGNATURE SERIES" description="Con un peso ligeramente inclinado hacia el extremo de la empuñadura, un mango negro mate limpio y un barril natural, el bate de maple MLB Prime KS12 de Kyle Schwarber es ideal para cualquier bateador poderoso." />
        </AppContainer>
    );
}

export default App;
