import React, { useState, useContext, useEffect } from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';
import { VictoryLine, VictoryScatter, VictoryChart, VictoryAxis } from 'victory-native';
import { EmailContext } from '../contexts/EmailContext';

const SemanalView = () => {
  const [data, setData] = useState([]);
  const { email } = useContext(EmailContext);

  useEffect(() => {
    fetch(`http://192.168.43.131:3000/api/semanal?correo=${email}`)
      .then(response => response.json())
      .then(result => {
        if (Array.isArray(result)) {
          const sortedData = result.sort((a, b) => new Date(a.fecha_hora) - new Date(b.fecha_hora));
          setData(sortedData);
        }
      })
      .catch(error => {
        console.error('Error al obtener los datos:', error);
      });
  }, [email]);

  // Take the last 15 records from the sorted data
  const last15Data = data.slice(-15);

  const convertToPST = (utcDate) => {
    const date = new Date(utcDate);
    const offset = date.getTimezoneOffset() + 600;
    date.setMinutes(date.getMinutes() + offset - 1440);
    return date.toISOString().slice(0, 19).replace('T', ' ');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Gráfica de Velocidades Semanales</Text>
      <VictoryChart>
        <VictoryLine
          data={last15Data.map((item, index) => ({
            x: index + 1,
            y: item.velocidadmedida,
          }))}
          style={{
            data: { stroke: 'rgba(204, 0, 0, 1)' },
          }}
        />
        <VictoryScatter
          data={last15Data.map((item, index) => ({
            x: index + 1,
            y: item.velocidadmedida,
          }))}
          size={5}
          style={{
            data: { fill: 'rgba(204, 0, 0, 1)' },
          }}
        />
        <VictoryAxis tickValues={Array.from({ length: 15 }, (_, i) => i + 1)} />
      </VictoryChart>
      <ScrollView style={styles.tableContainer}>
        <View style={styles.tableHeader}>
          <Text style={styles.headerText}>#</Text>
          <Text style={styles.headerText}>Fecha y Hora</Text>
          <Text style={styles.headerText}>Velocidad</Text>
        </View>
        {last15Data.map((item, index) => (
          <View key={index} style={styles.tableRow}>
            <Text style={styles.rowText}>{index + 1}</Text>
            <Text style={styles.rowText}>{convertToPST(item.fecha_hora)}</Text>
            <Text style={styles.rowText}>{item.velocidadmedida}</Text>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  tableContainer: {
    flex: 1,
    width: '100%',
  },
  tableHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    paddingVertical: 5,
    backgroundColor: '#f2f2f2',
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  tableRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    paddingVertical: 5,
  },
  rowText: {
    fontSize: 14,
  },
});

export default SemanalView;
