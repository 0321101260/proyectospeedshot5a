import React, { useState } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StyleSheet, Text, View } from 'react-native';
import LoadingScreen from './screens/LoadingScreen';
import { NavigationContainer } from '@react-navigation/native';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import { EmailContext } from './contexts/EmailContext';
import HomeScreen from './screens/HomeScreen';
import TrainingScreen from './screens/TrainingScreen';
import AccountScreen from './screens/AccountScreen';
import PitcherScreen from './screens/PitcherScreen';
import RecordsScreen from './screens/RecordsScreen';
import SemanalScreen from './screens/SemanalScreen';
import MensualScreen from './screens/MensualScreen';
import RecomendationsScreen from './screens/RecomendationsScreen';

const Stack = createNativeStackNavigator();

export default function App() {
  const [isLoading, setIsLoading] = React.useState(true);
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);
  const [email, setEmail] = useState('');
  
  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 3000);
  }, []);

  if (isLoading) {
    return <LoadingScreen />;
  }
  
  return (
    <EmailContext.Provider value={{ email, setEmail }}>
      <NavigationContainer>
        <Stack.Navigator>
          {!isLoggedIn ? (
            <>
              <Stack.Screen name="Login" component={LoginScreen} />
              <Stack.Screen name="Register" component={RegisterScreen} />
              <Stack.Screen name="Home" component={HomeScreen} />
              <Stack.Screen name="Training" component={TrainingScreen} />
              <Stack.Screen name="Pitcher" component={PitcherScreen} />
              <Stack.Screen name="Records" component={RecordsScreen} />
              <Stack.Screen name="Semanal" component={SemanalScreen} />
              <Stack.Screen name="Mensual" component={MensualScreen} />
              <Stack.Screen name="Recomendations" component={RecomendationsScreen} />
              <Stack.Screen name="Account" component={AccountScreen} />
              
            </> 
          ) : (
            <>
              
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </EmailContext.Provider>
  );
  
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
